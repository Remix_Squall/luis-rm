<?php session_start(); header("Content-Type: text/html;charset=utf-8"); ?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="src/style/inicio.css" />
		<link rel="shortcut icon" href="./src/img/favicon.ico" type="image/x-icon">
		<link rel="icon" href="./src/img/favicon.ico" type="image/x-icon">
		<title>Luis Romero Moreno</title>
	</head>
	<body>
		<header>
			<h1><hr /> Luis Romero Moreno <hr /></h1>
			<p><img src="src/img/lrmLogo.png"></p>
		</header>
		<nav>
			<table>
				<tr>
					<th>INICIO</th>
					<th>Portfolio</th>
					<th>Acerca de mi</th>
					<th>Contacto</th>
				</tr>
			</table>
		</nav>
		<section>
			
		</section>
		<aside>
			<figure>
				<img src="../src/img/administracion.png">
			</figure>
		</aside>
		<footer>
			Luis Romero Moreno &copy; 2014. Derechos reservados.
		</footer>
	</body>
</html>
