<?php
$receive=$_GET['page'];
if(!isset($receive)) {
	header( 'Location: old/' ) ;
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Luis Romero Moreno</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link rel="stylesheet" href="css/luisrm.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="nav">
			<div class="container">
				<ul class="pull-left">
					<li>
						<a href="?page=0">Luis Romero Moreno</a>
					</li>
					<li>
						<a href="?page=4">Mapa Web</a>
					</li>
				</ul>
				<ul class="pull-right">
					<li>
						<a href="?page=1">Sobre mí</a>
					</li>
					<li>
						<a href="?page=2">Proyectos</a>
					</li>
					<li>
						<a href="?page=3">Contacto</a>
					</li>
				</ul>
			</div>
		</div>
		<?php
		switch ($receive) {
			case '1':
				include('pages/about.php');
				break;
			case '2':
				include('pages/projects.php');
				break;
			case '3':
				include('pages/contact.php');
				break;
			case '4':
				include('pages/web-map.php');
				break;
			default:
				include('pages/main.php');
				break;
		}
		?>
		<div class="learn-more">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h3>Sobre la web</h3>
						<p>
							© Copyright 2014 Luis Romero Moreno. Algunos derechos reservados.
						</p>
						<p>
							Comprobadores web
						</p>
					</div>
					<div class="col-md-6">
						<h3>Redes Sociales</h3>
						<p>
							Renting out your unused space could pay your bills or fund your next vacation.
						</p>
						<p>
							<a href="#">Learn more about hosting</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>