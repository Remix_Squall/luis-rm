<!--<div class="jumbotron">
<div class="container">
<h1>Hello #Code</h1>
<p>
Creando arte y perfección a través de la práctica y el estudio.
</p>
<a href="#">Learn More</a>
</div>
</div>-->
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		<div class="item active">
			<img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Hello <code>#Code</code></h1>
					<p>
						Note: If you're viewing this page via a <code>
							file://</code>
						URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.
					</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Second slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Another example headline.</h1>
					<p>
						Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
					</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>One more for good measure.</h1>
					<p>
						Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
					</p>
				</div>
			</div>
		</div>
	</div>
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div><!-- /.carousel -->
<div class="neighborhood-guides">
	<div class="container">
		<h2>Neightborhood Guides</h2>
		<p>
			Not sure where to stay?
		</p>
		<div class="row">
			<div class="col-md-4">
				<div class="thumbnail">
					<img src="http://goo.gl/0sX3jq" />
				</div>
				<div class="thumbnail">
					<img src="http://goo.gl/an2HXY" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="thumbnail">
					<img src="http://goo.gl/Av1pac" />
				</div>
				<div class="thumbnail">
					<img src="http://goo.gl/vw43v1" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="thumbnail">
					<img src="http://goo.gl/0Kd7UO" />
				</div>
			</div>
		</div>
	</div>
</div>