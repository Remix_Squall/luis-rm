<?php

$tag = $_POST['tag'];

include_once ('../class.phpmailer.php');
include_once ('../class.smtp.php');
// optional, gets called from within class.phpmailer.php if not already loaded
require_once ('recaptchalib.php');

$privatekey = "6LezgP0SAAAAADkHsKPLg-fMLd7jR5UrUMF84W-7";
$resp = recaptcha_check_answer($privatekey, 
								$_SERVER["REMOTE_ADDR"], 
								$_POST["recaptcha_challenge_field"], 
								$_POST["recaptcha_response_field"]);

$mail = new PHPMailer();
$body = 'Dirección IP remota: ' . getRealIP() . '<br><br>';
$body .= $_POST['inputMessage'];

$mail -> IsSMTP();
// telling the class to use SMTP
$mail -> Host = "mail.yourdomain.com";
// SMTP server
$mail -> SMTPDebug = 0;
// enables SMTP debug information (for testing)
// 1 = errors and messages
// 2 = messages only
$mail -> SMTPAuth = true;
// enable SMTP authentication
$mail -> SMTPSecure = "ssl";
// sets the prefix to the servier
$mail -> Host = "smtp.gmail.com";
// sets GMAIL as the SMTP server
$mail -> Port = 465;
// set the SMTP port for the GMAIL server
$mail -> Username = "micorreo@gmail.com";
// GMAIL username
$mail -> Password = "contraseña";
// GMAIL password

$mail -> SetFrom($_POST['inputEmail'], $_POST['inputName']);

// $mail -> AddReplyTo("name@yourdomain.com", "First Last");

$mail -> isHTML(true);

$mail -> Subject = "Contacto Web Personal - " . $_POST['inputName'] . ' (' . $_POST['inputWeb'] . ')';

//$mail -> Body = $_POST['inputMessage'];
$mail -> msgHTML($body);

$mail -> AltBody = strip_tags(stripslashes($_POST['inputMessage']));
// optional, comment out and test

$address = "doctorluiswho@gmail.com";
$mail -> AddAddress($address, "Luis Romero Moreno - Profesional");

// $mail -> AddAttachment("images/phpmailer.gif");
// attachment
// $mail -> AddAttachment("images/phpmailer_mini.gif");
// attachment

if (isset($tag) && $tag !== '') {
	if ($tag == 'contact') {
		if ($mail -> Send() && $resp -> is_valid)
			echo true;
		echo false;
	}
}

function getRealIP() {

	if ($_SERVER['HTTP_X_FORWARDED_FOR'] != '') {
		$client_ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : ((!empty($_ENV['REMOTE_ADDR'])) ? $_ENV['REMOTE_ADDR'] : "unknown");

		// los proxys van añadiendo al final de esta cabecera
		// las direcciones ip que van "ocultando". Para localizar la ip real
		// del usuario se comienza a mirar por el principio hasta encontrar
		// una dirección ip que no sea del rango privado. En caso de no
		// encontrarse ninguna se toma como valor el REMOTE_ADDR

		$entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);

		reset($entries);
		while (list(, $entry) = each($entries)) {
			$entry = trim($entry);
			if (preg_match("/^([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)/", $entry, $ip_list)) {
				// http://www.faqs.org/rfcs/rfc1918.html
				$private_ip = array('/^0\\./', '/^127\\.0\\.0\\.1/', '/^192\\.168\\..*/', '/^172\\.((1[6-9])|(2[0-9])|(3[0-1]))\\..*/', '/^10\\..*/');

				$found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);

				if ($client_ip != $found_ip) {
					$client_ip = $found_ip;
					break;
				}
			}
		}
	} else {
		$client_ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : ((!empty($_ENV['REMOTE_ADDR'])) ? $_ENV['REMOTE_ADDR'] : "unknown");
	}

	return $client_ip;

}
?>