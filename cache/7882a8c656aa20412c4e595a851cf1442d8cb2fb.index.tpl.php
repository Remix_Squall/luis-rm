<?php /*%%SmartyHeaderCode:1305396478541c12e9ad5e54-07458669%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7882a8c656aa20412c4e595a851cf1442d8cb2fb' => 
    array (
      0 => './templates/index.tpl',
      1 => 1415709116,
      2 => 'file',
    ),
    'dcf64f36a8afd5f887bb61bf9b6a5f77c9e60d5f' => 
    array (
      0 => './configs/test.conf',
      1 => 1411126413,
      2 => 'file',
    ),
    '9e783ec3f9b639d4769c4c8a4acebf276361768a' => 
    array (
      0 => './templates/header.tpl',
      1 => 1415661937,
      2 => 'file',
    ),
    '0d2b19df54e3b720bcd57edd6b1307d80487ceeb' => 
    array (
      0 => './templates/top/top.tpl',
      1 => 1412851867,
      2 => 'file',
    ),
    '9127682379a51dcd48ea960b59f70c321f63cfed' => 
    array (
      0 => './templates/top/logo.tpl',
      1 => 1412956095,
      2 => 'file',
    ),
    '5d2a19ed489a4265e8706c807e5cb65148991952' => 
    array (
      0 => './templates/top/navigator.tpl',
      1 => 1415714872,
      2 => 'file',
    ),
    'e62c0cac80f80b5947f57efe8e3ae192535689b0' => 
    array (
      0 => './templates/content/content.tpl',
      1 => 1413055075,
      2 => 'file',
    ),
    '419c68247af336c6ea0f03da53d15254add2a31c' => 
    array (
      0 => './templates/content/carousel.tpl',
      1 => 1415724167,
      2 => 'file',
    ),
    '5a5ca1dd63bae69157d28fc5168e3139f1d559cb' => 
    array (
      0 => './templates/content/about.tpl',
      1 => 1412954649,
      2 => 'file',
    ),
    '3e526862e47a6eba8ba181b4ac5170786f193ea2' => 
    array (
      0 => './templates/content/projects.tpl',
      1 => 1415714940,
      2 => 'file',
    ),
    '670e70ebc8c637c04d459fb2df36a3bef76e7e23' => 
    array (
      0 => './templates/content/contact.tpl',
      1 => 1415671494,
      2 => 'file',
    ),
    '4b48750b9463b5b2be97d918dbfd9b1f69ef319a' => 
    array (
      0 => './templates/footer/footer.tpl',
      1 => 1415714272,
      2 => 'file',
    ),
    '4bcc9adf03159f64af2327a33919b29eed088828' => 
    array (
      0 => './templates/footer/upfooter.tpl',
      1 => 1415711713,
      2 => 'file',
    ),
    '62f32a8da8540639cfd9cc273b9e06db920f4ad4' => 
    array (
      0 => './templates/footer/about.tpl',
      1 => 1415711082,
      2 => 'file',
    ),
    'cd70fb09b40e0e4a7cf4440065489933715c817f' => 
    array (
      0 => './templates/footer/tags.tpl',
      1 => 1415711410,
      2 => 'file',
    ),
    '1c6b3f419a9cac35014c73c18f4f8a62cafe2b2a' => 
    array (
      0 => './templates/footer/social.tpl',
      1 => 1415714045,
      2 => 'file',
    ),
    'b1a79e8cef1a8caa4a397b9d012c1b48c7d30ba0' => 
    array (
      0 => './templates/footer/subfooter.tpl',
      1 => 1415714554,
      2 => 'file',
    ),
    '51f6e01565fa80618d77c9c4eb1c30a125547d24' => 
    array (
      0 => './templates/footer.tpl',
      1 => 1415660530,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1305396478541c12e9ad5e54-07458669',
  'cache_lifetime' => 120,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_54623cee6087a8_24198604',
  'has_nocache_code' => true,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54623cee6087a8_24198604')) {function content_54623cee6087a8_24198604($_smarty_tpl) {?>
<HTML>
<HEAD>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <TITLE>Luis Romero Moreno - Web Personal</TITLE>
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/personal.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.css" />
	<link rel="stylesheet" href="css/igrowl.min.css" />
    <link rel="stylesheet" href="css/fonts/linecons.css" /> <!-- Linecons CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</HEAD>
<BODY data-spy="scroll" data-target=".navegador">


<div class="container-fluid logo">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<img src="img/logo.png" alt="Luis Romero" class="img-responsive">
			</div>
			<div class="col-md-10">
				<h1>Hello World! ¡Soy Luis Romero!</h1>
				<h2>Programador Freelance Web y Android desde Sevilla, España</h2>
			</div>
		</div>
	</div>
</div>


<div id="nav">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegador">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navegador">
				<ul class="nav navbar-nav">
					<li>
						<a href="#top">Inicio</a>
					</li>
					<li>
						<a href="#about">Sobre mí</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Proyectos <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="#calculadora">Calculadora</a>
							</li>
							<li>
								<a href="#fuentes">Fuentes de Sevilla</a>
							</li>
						</ul>
					</li>
					<li><a href="#contact">Contacto</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
</div>

<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		<div class="item active">
			<img src="img/test3.png" alt="First slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Arte y perfección</h1>
					<p>
						Me encanta ser muy meticuloso y cuidadoso con los fallos, y a ser posible refinar todo lo que pueda de código.
					</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/test3.png" alt="Second slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Adaptabilidad Asegurada</h1>
					<p>
						No importa el lenguaje o la forma de trabajar, yo me adapto con mucha rapidez y sensillez.
					</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/test3.png" alt="Third slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Trabajo en equipo</h1>
					<p>
						Me coordino muy bien con mis compañeros en los proyectos en los que trabajo.
					</p>
				</div>
			</div>
		</div>
	</div>
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div><!-- /.carousel -->

<div id="about" class="container about">
	<h1>Sobre mí</h1>
	<h2>Amplia experiencia en informática</h2>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="thumbnail">
				<img src="img/yo_cv.jpg" class="img-circle img-responsive">
				<div class="caption">
					<h3>Luis Romero Moreno</h3>
					<h4>Programador Web y Móviles</h4>
					<p>Durante estos últimos meses ha estado trabajando como Freelance con diversos proyectos web (red social, Joomla, etc).</p>
					<p>Dedica sus horas, tanto de trabajo como de ocio, a ampliar sus conocimientos sobre nuevos lenguajes y nuevas técnicas de programación.</p>
					<p>Controla diversos lenguajes, tanto de escritorio como móviles (Python, Java, Android, C#, etc).</p>
				</div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</div>


<div id="projects" class="container">
	<h1>Proyectos</h1>
	<div class="row">
		<div class="col-md-6" id="calculadora">
			<div class="thumbnail">
				<img src="img/android-app.png" class="img-rounded img-responsive">
				<div class="caption">
					<h3>Calculadora Android</h3>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
							0%
						</div>
					</div>
					<p>Calculadora científica para Android.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6" id="fuentes">
			<div class="thumbnail">
				<img src="img/android-app.png" class="img-rounded img-responsive">
				<div class="caption">
					<h3>Fuentes de Sevilla</h3>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
							0%
						</div>
					</div>
					<p>Geolocalización de fuentes de agua en Sevilla para Android.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="contact" class="container">
	<h1>Contacto</h1>
	<h2>Si tienes alguna idea o sugerencia o quieres tener un currículum más completo, por favor, escríbeme aquí abajo.</h2>
	<h2>Puedes encontrarme también en Twitter o Facebook.</h2>
	<form id="contactForm" role="form">
		<div class="form-group">
			<label for="inputName">Nombre*</label>
			<input type="text" class="form-control" id="inputName" name="inputName" required="required" data-validation-message="Por favor, introduzca su nombre" placeholder="Introduzca su nombre" />
		</div>
		<div class="form-group">
			<label for="inputEmail">Dirección Email*</label>
			<input type="email" class="form-control" id="inputEmail" name="inputEmail" required="required" data-validation-message="Por favor, introduzca su email" placeholder="Introduzca su Email" />
		</div>
		<div class="form-group">
			<label for="inputWeb">Sitio Web</label>
			<input type="url" class="form-control" id="inputWeb" name="inputWeb" placeholder="Introduzca su sitio web" />
		</div>
		<div class="form-group">
			<label for="inputMessage">Mensaje*</label>
			<textarea class="form-control" id="inputMessage" name="inputMessage" cols="10" rows="3" required="required" data-validation-message="Por favor introduzca su mensaje" min-length="5" data-validation-minlength-message="Mínimo 5 caracteres" placeholder="Mensaje" rows="3"></textarea>
		</div>
		<div class="form-group">
			<label for="captcha">&nbsp;</label>
			<?php echo $_smarty_tpl->tpl_vars['recaptcha']->value;?>

		</div>
		<button type="submit" class="btn btn-default">
			Enviar
		</button>
		<div id="loading"><i class="fa fa-refresh fa-spin"></i></div>
		<button type="reset" class="btn btn-default">
			Restablecer
		</button>
	</form>
	<span></span>
</div>



<div class="container-fluid footer">
	<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="media">
	<a class="pull-left" href="#top">
		<img src="./img/logo.png" alt="Logo LR" title="Ir al inicio" />
	</a>
	<div class="media-body">
		<h4 class="media-heading">Acerca de la Web</h4>
		La web ha sido creada con Smarty Template, junto con otros elementos como Bootstrap para adquirir Responsive Design.
	</div>
</div>

		</div>
		<div class="col-md-4">
			<h4>Etiquetas</h4>
<span class="label label-info">Android</span>
<span class="label label-info">PHP</span>
<span class="label label-info">Java</span>
<span class="label label-info">JavaScript</span>
<span class="label label-info">MVC</span>
<span class="label label-info">ORM</span>
<span class="label label-info">C#</span>
<span class="label label-info">Microsoft Office</span>
<span class="label label-info">Linux</span>
<span class="label label-info">HTML5</span>
<span class="label label-info">Responsive Design</span>
<span class="label label-info">J2EE</span>
<span class="label label-info">MySQL</span>
<span class="label label-info">Oracle</span>
<span class="label label-info">JDBC</span>
<span class="label label-info">Git</span>
<span class="label label-info">SVN</span>
<span class="label label-info">JSP</span>

		</div>
		<div class="col-md-4">
			<h4>Redes Sociales</h4>
<a href="https://google.com/+LuisRomeroMoreno" title="Ir a Google Plus"><i class="fa fa-google-plus fa-2x"></i></a>
<a href="https://es.linkedin.com/pub/luis-romero-moreno/45/b65/821/" title="Ir a LinkedIn"><i class="fa fa-linkedin fa-2x"></i></a>
<a href="https://www.facebook.com/Remix.Squall" title="Ir a Facebook"><i class="fa fa-facebook fa-2x"></i></a>
<a href="https://twitter.com/DoctorLuisWho" title="Ir a Twitter"><i class="fa fa-twitter fa-2x"></i></a>

		</div>
	</div>
</div>

</div>
<div class="container-fluid design">
	<div class="container">
	<p>&copy; 2014 Luis Romero Moreno | Diseñado por Luis Romero Moreno</p>
</div>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.js"></script>
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/igrowl.min.js"></script>
<script src="js/personal.js"></script>
</BODY>
</HTML>
<?php }} ?>
