<div class="container-fluid logo">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<img src="img/logo.png" alt="Luis Romero" class="img-responsive">
			</div>
			<div class="col-md-10">
				<h1>Hello World! ¡Soy Luis Romero!</h1>
				<h2>Programador Freelance Web y Android desde Sevilla, España</h2>
			</div>
		</div>
	</div>
</div>
