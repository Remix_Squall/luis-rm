<div id="nav">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegador">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navegador">
				<ul class="nav navbar-nav">
					<li>
						<a href="#top">Inicio</a>
					</li>
					<li>
						<a href="#about">Sobre mí</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Proyectos <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="#calculadora">Calculadora</a>
							</li>
							<li>
								<a href="#fuentes">Fuentes de Sevilla</a>
							</li>
						</ul>
					</li>
					<li><a href="#contact">Contacto</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
</div>