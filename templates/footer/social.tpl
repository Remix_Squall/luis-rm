<h4>Redes Sociales</h4>
<a href="https://google.com/+LuisRomeroMoreno" title="Ir a Google Plus"><i class="fa fa-google-plus fa-2x"></i></a>
<a href="https://es.linkedin.com/pub/luis-romero-moreno/45/b65/821/" title="Ir a LinkedIn"><i class="fa fa-linkedin fa-2x"></i></a>
<a href="https://www.facebook.com/Remix.Squall" title="Ir a Facebook"><i class="fa fa-facebook fa-2x"></i></a>
<a href="https://twitter.com/DoctorLuisWho" title="Ir a Twitter"><i class="fa fa-twitter fa-2x"></i></a>
