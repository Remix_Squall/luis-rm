<div class="container">
	<div class="row">
		<div class="col-md-4">
			{include file="footer/about.tpl"}
		</div>
		<div class="col-md-4">
			{include file="footer/tags.tpl"}
		</div>
		<div class="col-md-4">
			{include file="footer/social.tpl"}
		</div>
	</div>
</div>
