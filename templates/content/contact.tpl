<div id="contact" class="container">
	<h1>Contacto</h1>
	<h2>Si tienes alguna idea o sugerencia o quieres tener un currículum más completo, por favor, escríbeme aquí abajo.</h2>
	<h2>Puedes encontrarme también en Twitter o Facebook.</h2>
	<form id="contactForm" role="form">
		<div class="form-group">
			<label for="inputName">Nombre*</label>
			<input type="text" class="form-control" id="inputName" name="inputName" required="required" data-validation-message="Por favor, introduzca su nombre" placeholder="Introduzca su nombre" />
		</div>
		<div class="form-group">
			<label for="inputEmail">Dirección Email*</label>
			<input type="email" class="form-control" id="inputEmail" name="inputEmail" required="required" data-validation-message="Por favor, introduzca su email" placeholder="Introduzca su Email" />
		</div>
		<div class="form-group">
			<label for="inputWeb">Sitio Web</label>
			<input type="url" class="form-control" id="inputWeb" name="inputWeb" placeholder="Introduzca su sitio web" />
		</div>
		<div class="form-group">
			<label for="inputMessage">Mensaje*</label>
			<textarea class="form-control" id="inputMessage" name="inputMessage" cols="10" rows="3" required="required" data-validation-message="Por favor introduzca su mensaje" min-length="5" data-validation-minlength-message="Mínimo 5 caracteres" placeholder="Mensaje" rows="3"></textarea>
		</div>
		<div class="form-group">
			<label for="captcha">&nbsp;</label>
			{nocache}{$recaptcha}{/nocache}
		</div>
		<button type="submit" class="btn btn-default">
			Enviar
		</button>
		<div id="loading"><i class="fa fa-refresh fa-spin"></i></div>
		<button type="reset" class="btn btn-default">
			Restablecer
		</button>
	</form>
	<span></span>
</div>
