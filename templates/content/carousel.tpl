<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		<div class="item active">
			<img src="img/test3.png" alt="First slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Arte y perfección</h1>
					<p>
						Me encanta ser muy meticuloso y cuidadoso con los fallos, y a ser posible refinar todo lo que pueda de código.
					</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/test3.png" alt="Second slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Adaptabilidad Asegurada</h1>
					<p>
						No importa el lenguaje o la forma de trabajar, yo me adapto con mucha rapidez y sensillez.
					</p>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/test3.png" alt="Third slide">
			<div class="container">
				<div class="carousel-caption">
					<h1>Trabajo en equipo</h1>
					<p>
						Me coordino muy bien con mis compañeros en los proyectos en los que trabajo.
					</p>
				</div>
			</div>
		</div>
	</div>
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div><!-- /.carousel -->