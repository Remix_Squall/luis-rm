<div id="about" class="container about">
	<h1>Sobre mí</h1>
	<h2>Amplia experiencia en informática</h2>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="thumbnail">
				<img src="img/yo_cv.jpg" class="img-circle img-responsive">
				<div class="caption">
					<h3>Luis Romero Moreno</h3>
					<h4>Programador Web y Móviles</h4>
					<p>Durante estos últimos meses ha estado trabajando como Freelance con diversos proyectos web (red social, Joomla, etc).</p>
					<p>Dedica sus horas, tanto de trabajo como de ocio, a ampliar sus conocimientos sobre nuevos lenguajes y nuevas técnicas de programación.</p>
					<p>Controla diversos lenguajes, tanto de escritorio como móviles (Python, Java, Android, C#, etc).</p>
				</div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</div>
