<div id="projects" class="container">
	<h1>Proyectos</h1>
	<div class="row">
		<div class="col-md-6" id="calculadora">
			<div class="thumbnail">
				<img src="img/android-app.png" class="img-rounded img-responsive">
				<div class="caption">
					<h3>Calculadora Android</h3>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
							0%
						</div>
					</div>
					<p>Calculadora científica para Android.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6" id="fuentes">
			<div class="thumbnail">
				<img src="img/android-app.png" class="img-rounded img-responsive">
				<div class="caption">
					<h3>Fuentes de Sevilla</h3>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
							0%
						</div>
					</div>
					<p>Geolocalización de fuentes de agua en Sevilla para Android.</p>
				</div>
			</div>
		</div>
	</div>
</div>