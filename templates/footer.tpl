<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.js"></script>
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/igrowl.min.js"></script>
<script src="js/personal.js"></script>
<script>
  (function(i,s,o,g,r,a,m){ldelim}i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ldelim}
  (i[r].q=i[r].q||[]).push(arguments){rdelim},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  {rdelim})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44937069-1', 'auto');
  ga('send', 'pageview');

</script>
</BODY>
</HTML>
