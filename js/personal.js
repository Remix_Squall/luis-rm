// $('body').scrollspy({ target: '#nav' });

$(document).ready(function() {
	
	$(window).scroll(function() {
		if($(window).scrollTop() > $('.logo').height()) {
			$('#nav').addClass('navbar-fixed-top');
			$('#nav').removeClass('navbar-static-top');
		} else {
			$('#nav').removeClass('navbar-fixed-top');
			$('#nav').addClass('navbar-static-top');
		}
	});

	$('form').submit(function(e) {
		e.preventDefault();
		var data = $(this).serializeArray();
		data.push({name: 'tag', value: 'contact'});
		$.ajax({
			url: './controllers/process.php',
			type: 'post',
			dataType: 'json',
			data: data,
			beforeSend: function() {
				$('#loading').css('display','inline');
			}
		})
		.done(function() {
			$('form').trigger('reset');
			$.iGrowl({
				message: "El mensaje fue enviado correctamente",
				type: 'success',
				icon: 'linecons-bulb',
				placement : {
					x: 'center',
					y: 'bottom'
				}
			})
		})
		.fail(function() {
			$.iGrowl({
				message: "¡No ha introducido el CAPTCHA correctamente!",
				type: 'error',
				icon: 'linecons-fire',
				placement : {
					x: 'center',
					y: 'bottom'
				}
			})
		})
		.always(function() {
			$('#loading').hide();
		});
		
	});

});

$('body').scrollspy({ target: '.navegador' });