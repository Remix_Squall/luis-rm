<?php
/**
 * Example Application
 *
 * @package Example-application
 */

require 'libs/Smarty.class.php';
require_once 'controllers/recaptchalib.php';

$publickey = "6LezgP0SAAAAAFo5zLQ5yOQBwUcYQTQOqMEvHlar";

$smarty = new Smarty;

if(!$_POST) {
	$smarty -> assign("recaptcha", recaptcha_get_html($publickey));
}

//$smarty->force_compile = true;
$smarty->debugging = false;
$smarty->caching = true;
$smarty->cache_lifetime = 120;

$smarty->display('index.tpl');