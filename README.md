# Luis RM - Página Web Personal #

Es un proyecto que he ido desechando continuamente debido a que encontraba recursos más útiles y mejores hasta ahora. Por lo que está creado a partir de muchos recursos tales como Bootstrap y Smarty Template.

## Recursos utilizados ##

* Twitter Bootstrap (http://getbootstrap.com)
* Smarty Template (http://www.smarty.net)
* jQuery, JS y AJAX.
* Font Awesome (http://fortawesome.github.io)
* iGrowl (http://catc.github.io/iGrowl)
* PHPMailer (http://github.com/Syncro/PHPMailer)
* Recaptcha (http://www.google.com/recaptcha/intro/)
* Google Analytics (http://www.google.com/analytics)

## ¿Cómo se instala? ##

### Modificar parámetros correo electrónico ###

En la ruta "controllers/process.php" hay que modificar los parámetros de correo desde donde se envía y que son el usuario y la contraseña de GMail.

### Modificar parámetros ReCaptcha ###

Dependiendo del hosting que vayas a utilizar las claves (privada y pública) cambian, así que debes cambiar la pública en "index.php" y la privada en "controllers/process.php".

### Modificar código Analytics (Opcional) ###

Al igual que ReCaptcha, hay que modificar una única clave que se obtiene de la web de Analytics por el del dominio. Esta clave o código se modifica en "templates/footer.tpl".

### Subir todo a un FTP ###

Subir todos los ficheros vía FTP (FileZilla) del hosting que vayas a usar.

## Futuros Cambios ##

* Nuevas fotografías en el carrousel y la del autor.
* Revisión de código.
* Internacionalización.

## Contactar con ##

* Remix_Squall (autor del repositorio)