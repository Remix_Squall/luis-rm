<?php /* Smarty version Smarty-3.1.19, created on 2014-11-11 15:08:22
         compiled from "./templates/top/navigator.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2005532342541c171605d2e3-80949834%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5d2a19ed489a4265e8706c807e5cb65148991952' => 
    array (
      0 => './templates/top/navigator.tpl',
      1 => 1415714872,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2005532342541c171605d2e3-80949834',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_541c171605fa96_69106143',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_541c171605fa96_69106143')) {function content_541c171605fa96_69106143($_smarty_tpl) {?><div id="nav">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegador">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navegador">
				<ul class="nav navbar-nav">
					<li>
						<a href="#top">Inicio</a>
					</li>
					<li>
						<a href="#about">Sobre mí</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Proyectos <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="#calculadora">Calculadora</a>
							</li>
							<li>
								<a href="#fuentes">Fuentes de Sevilla</a>
							</li>
						</ul>
					</li>
					<li><a href="#contact">Contacto</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
</div><?php }} ?>
