<?php /* Smarty version Smarty-3.1.19, created on 2014-11-11 03:04:56
         compiled from "./templates/content/contact.tpl" */ ?>
<?php /*%%SmartyHeaderCode:939583035439857d313849-59885745%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '670e70ebc8c637c04d459fb2df36a3bef76e7e23' => 
    array (
      0 => './templates/content/contact.tpl',
      1 => 1415671494,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '939583035439857d313849-59885745',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5439857d315064_60789217',
  'variables' => 
  array (
    'recaptcha' => 1,
  ),
  'has_nocache_code' => true,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5439857d315064_60789217')) {function content_5439857d315064_60789217($_smarty_tpl) {?><div id="contact" class="container">
	<h1>Contacto</h1>
	<h2>Si tienes alguna idea o sugerencia o quieres tener un currículum más completo, por favor, escríbeme aquí abajo.</h2>
	<h2>Puedes encontrarme también en Twitter o Facebook.</h2>
	<form id="contactForm" role="form">
		<div class="form-group">
			<label for="inputName">Nombre*</label>
			<input type="text" class="form-control" id="inputName" name="inputName" required="required" data-validation-message="Por favor, introduzca su nombre" placeholder="Introduzca su nombre" />
		</div>
		<div class="form-group">
			<label for="inputEmail">Dirección Email*</label>
			<input type="email" class="form-control" id="inputEmail" name="inputEmail" required="required" data-validation-message="Por favor, introduzca su email" placeholder="Introduzca su Email" />
		</div>
		<div class="form-group">
			<label for="inputWeb">Sitio Web</label>
			<input type="url" class="form-control" id="inputWeb" name="inputWeb" placeholder="Introduzca su sitio web" />
		</div>
		<div class="form-group">
			<label for="inputMessage">Mensaje*</label>
			<textarea class="form-control" id="inputMessage" name="inputMessage" cols="10" rows="3" required="required" data-validation-message="Por favor introduzca su mensaje" min-length="5" data-validation-minlength-message="Mínimo 5 caracteres" placeholder="Mensaje" rows="3"></textarea>
		</div>
		<div class="form-group">
			<label for="captcha">&nbsp;</label>
			<?php echo '/*%%SmartyNocache:939583035439857d313849-59885745%%*/<?php echo $_smarty_tpl->tpl_vars[\'recaptcha\']->value;?>
/*/%%SmartyNocache:939583035439857d313849-59885745%%*/';?>

		</div>
		<button type="submit" class="btn btn-default">
			Enviar
		</button>
		<div id="loading"><i class="fa fa-refresh fa-spin"></i></div>
		<button type="reset" class="btn btn-default">
			Restablecer
		</button>
	</form>
	<span></span>
</div>
<?php }} ?>
