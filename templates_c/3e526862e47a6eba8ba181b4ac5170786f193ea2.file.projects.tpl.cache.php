<?php /* Smarty version Smarty-3.1.19, created on 2014-11-11 15:09:02
         compiled from "./templates/content/projects.tpl" */ ?>
<?php /*%%SmartyHeaderCode:793757765437fb88cc3a36-73580586%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e526862e47a6eba8ba181b4ac5170786f193ea2' => 
    array (
      0 => './templates/content/projects.tpl',
      1 => 1415714940,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '793757765437fb88cc3a36-73580586',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5437fb88cc5112_68979125',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5437fb88cc5112_68979125')) {function content_5437fb88cc5112_68979125($_smarty_tpl) {?><div id="projects" class="container">
	<h1>Proyectos</h1>
	<div class="row">
		<div class="col-md-6" id="calculadora">
			<div class="thumbnail">
				<img src="img/android-app.png" class="img-rounded img-responsive">
				<div class="caption">
					<h3>Calculadora Android</h3>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
							0%
						</div>
					</div>
					<p>Calculadora científica para Android.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6" id="fuentes">
			<div class="thumbnail">
				<img src="img/android-app.png" class="img-rounded img-responsive">
				<div class="caption">
					<h3>Fuentes de Sevilla</h3>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
							0%
						</div>
					</div>
					<p>Geolocalización de fuentes de agua en Sevilla para Android.</p>
				</div>
			</div>
		</div>
	</div>
</div><?php }} ?>
