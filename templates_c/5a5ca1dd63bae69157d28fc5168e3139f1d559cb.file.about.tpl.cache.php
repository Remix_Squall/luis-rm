<?php /* Smarty version Smarty-3.1.19, created on 2014-10-10 17:24:13
         compiled from "./templates/content/about.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18765262945437d1a6cfe2c2-98285682%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5a5ca1dd63bae69157d28fc5168e3139f1d559cb' => 
    array (
      0 => './templates/content/about.tpl',
      1 => 1412954649,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18765262945437d1a6cfe2c2-98285682',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5437d1a6d009b2_21519524',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5437d1a6d009b2_21519524')) {function content_5437d1a6d009b2_21519524($_smarty_tpl) {?><div id="about" class="container about">
	<h1>Sobre mí</h1>
	<h2>Amplia experiencia en informática</h2>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="thumbnail">
				<img src="img/yo_cv.jpg" class="img-circle img-responsive">
				<div class="caption">
					<h3>Luis Romero Moreno</h3>
					<h4>Programador Web y Móviles</h4>
					<p>Durante estos últimos meses ha estado trabajando como Freelance con diversos proyectos web (red social, Joomla, etc).</p>
					<p>Dedica sus horas, tanto de trabajo como de ocio, a ampliar sus conocimientos sobre nuevos lenguajes y nuevas técnicas de programación.</p>
					<p>Controla diversos lenguajes, tanto de escritorio como móviles (Python, Java, Android, C#, etc).</p>
				</div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</div>
<?php }} ?>
